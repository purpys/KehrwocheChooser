namespace KehrwocheChooser;

public class Chooser
{
    private record Pair(int First, int Second)
    {
        public int First { get; set; } = First;
        public int Second { get; set; } = Second;

        public static Pair MakePair(int first, int second) => new(first, second);
    }

#if DEBUG
    public static bool PrintDictionary = false;
#endif
    private readonly Dictionary<string, Pair> _availablePeople = new();

    public Chooser(IReadOnlyList<string> kehrwocheList)
    {
        AddAvailablePeople(kehrwocheList);

#if DEBUG
        if (PrintDictionary) Print();
#endif
    }

    /// <summary>
    /// Calculates a Score based on the Number of Finished Kehrwochen and when the last Finished Kehrwoche was done
    /// </summary>
    /// <param name="nbOfKehrwocheDone">Number of times a person has done Kerhwoche</param>
    /// <param name="lastTimeKehrwocheWasDone">Number since a person has done Kehrwoche last</param>
    /// <returns>Score that describes how little a person needs to do Kehrwoche</returns>
    private static int CalculateScore(int nbOfKehrwocheDone, int lastTimeKehrwocheWasDone)
        => (nbOfKehrwocheDone * 2) + lastTimeKehrwocheWasDone;


    /// <summary>
    /// Chooses, based on a Score, the next Person who has to do Kehrwoche
    /// </summary>
    /// <returns>Name of the next Person to do the Kehrwoche</returns>
    public string Choose()
    {
        // Dictionary that holds the KeyValuePairs of _availablePeople and a Score
        // The Score is used to decide who has to do the next Kehrwoche
        var tmp = _availablePeople.ToDictionary(person => KeyValuePair.Create(person.Key, person.Value), _ => 0);

        foreach (var entry in tmp)
        {
            tmp[entry.Key] = CalculateScore(entry.Key.Value.First, entry.Key.Value.Second);
        }

#if DEBUG
        if (PrintDictionary)
        {
            const string headerTabs = "        ";
            const string headerName = "[NAME]:" + headerTabs;
            const string headerScore = "[SCORE]";
            Console.Error.WriteLine($"{headerName}{headerScore}");
            foreach (var (key, score) in tmp)
            {
                var name = key.Key;
                var tab = new string(' ', headerName.Length - name.Length);

                Console.Error.WriteLine($"{name}:{tab}{score}");
            }
        }
#endif


        // Returns the Name of the Person with the lowest Score
        return tmp.MinBy(kvp => kvp.Value).Key.Key;
    }


    private void AddAvailablePeople(IReadOnlyList<string> kehrwocheList)
    {
        for (var i = 0; i < kehrwocheList.Count; ++i)
        {
            var entry = kehrwocheList[i];
            if (_availablePeople.TryAdd(entry, Pair.MakePair(1, i)))
                continue;

            var tmp = _availablePeople[entry];
            tmp.First += 1;
            tmp.Second = i;
        }
    }

#if DEBUG

    public void Print()
    {
        foreach (var i in _availablePeople)
        {
            Console.Error.WriteLine(i.ToString());
        }
    }

#endif
}