﻿using KehrwocheChooser;

var kehrwocheList = args.ToList();

if (args.Length == 1)
{
    if (!File.Exists(args[0]))
        return -1;
    kehrwocheList = File.ReadAllLines(args[0]).ToList();
}

// Console.WriteLine($"Next Person in line is: {new Chooser(kehrwocheList).Choose()}");

const int max = 30;

for (var i = 0; i < max; ++i)
{
    Chooser.PrintDictionary = (i % 10) switch
    {
        0 => true,
        _ => false
    };
    Console.WriteLine($"\nIteration {i + 1}");
    var nextPersonInLine = new Chooser(kehrwocheList).Choose();
    Console.WriteLine($"Next Person in line is: {nextPersonInLine}");
    kehrwocheList.Add(nextPersonInLine);
    if (i == max - 2)
        Chooser.PrintDictionary = true;
}

return 0;